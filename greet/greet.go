// Package greet provides a hello function
package greet

import (
	"gitlab.com/dsearles/stringutil"
)

// Hello will greet ya
func Hello() string {
	return stringutil.Reverse("!oG ,olleH")
}
