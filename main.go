package main

import (
	"gitlab.com/dsearles/private-golang-deps/greet"
)

func main() {
	greet.Hello()
}
